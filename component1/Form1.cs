using  System.Text.RegularExpressions;
namespace component1
{

    public partial class Form1 : Form
    {
        Parser parser;
        private string? _filePath = null;
        private bool _fileSaved = true;
        private bool _runProgram = true;
        private int[] _loopList = new int[10];
        private int _loopIndex = -1;
        private bool _methodRunning = true;
        private int _tempProgramIndex = -1;

        Graphics g;
        public Form1()
        {
            InitializeComponent();
            g = pictureBox1.CreateGraphics();
            parser = new Parser(g);
        }
        LogicParser logicParser = new LogicParser();

        private void resetAll()
        {
            _loopIndex = -1;
            _runProgram = true;
            Array.Clear(_loopList, 0, 10);
            Variable.Clear();
        }
        /// <summary>
        /// Picturebox to picturize
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }
        /// <summary>
        /// Sender to send the file to save in a pc
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void save_Click(object sender, EventArgs e)
        {
            saveFile();
        }
        /// <summary>
        /// Run button to run the desired program
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
  

        private void run_Click(object sender, EventArgs e)
        {
            resetAll();
            for (int i = 0; i < textBox1.Lines.Length; i++)
            {
                try
                {
                    char[] seperator = new char[] { ' ' };
                    string[] args = textBox1.Lines[i].Trim().Split(seperator, 2);
                    string command = args[0].ToLower();
                    string parameters = "";
                    if (args.Length > 1)
                    {
                        parameters = args[1];
                    }
                    if (command == "while")
                    {
                        _loopList[++_loopIndex] = i;
                        continue;
                    }
                    else if (command == "endwhile")
                    {
                        if (_loopIndex > -1)
                        {
                            string[] commandParse = textBox1.Lines[_loopList[_loopIndex]].Trim().Split(seperator, 2);
                            string parsedCommand = commandParse[0].ToLower();
                            string parsedParameters = "";
                            if (commandParse.Length > 1)
                            {
                                parsedParameters = commandParse[1];
                            }
                            string condition = (parsedParameters.Trim());
                            if (logicParser.Parse(condition))
                            {
                                i = _loopList[_loopIndex];
                            }
                            else
                                _loopIndex--;
                        }
                        continue;
                    }
                    else if (command == "if")
                    {
                        Regex pattern = new Regex(@"(.+?) then (.+)");
                        Match match = pattern.Match(parameters.Trim());
                        if (match.Success) 
                        {
                            if (logicParser.Parse(match.Groups[1].Value))
                            {
                                parser.parse(match.Groups[2].Value.Trim());
                            }
                            continue;
                        }
                        string condition = (parameters.Trim());
                        if (!logicParser.Parse(condition))
                        {
                            _runProgram = false;
                        }
                        continue;
                    }
                    else if (command == "endif")
                    {
                        _runProgram = true;
                        continue;
                    }
                    else if (command == "method")
                    {
                        if (parameters != null)
                        {
                            _methodRunning = true;
                            if (Function.Funcs.ContainsKey(command))
                            {
                                MessageBox.Show("Function Already Exists", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                break;
                            }
                            else
                            {
                                try
                                {
                                    Function.Funcs.Add(parameters.Split(',')[0].Trim(), i);
                                }
                                catch (Exception)
                                {
                                    MessageBox.Show("Error defining Function", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    break;
                                }
                            }
                            continue;
                        }
                    }
                    else if (command == "endmethod")
                    {
                        i = _tempProgramIndex;
                        _tempProgramIndex = 0;
                        continue;
                    }
                    else if (command == "call") 
                    {
                        _tempProgramIndex = i;
                        i = Function.SearchFunction(parameters.Split(',')[0].Trim());
                        continue;
                    }
                    else
                    {
                        if (_runProgram)
                        {
                            parser.parse(textBox1.Lines[i].Trim());
                        }
                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    break;
                }
            }
        }

        private void fileToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void helpToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Developed By:-\nARJUN SHRESTHA\n", "About", MessageBoxButtons.OK);
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            g.Clear(Color.Gray);
            textBox2.Text = "";
            textBox1.Text = "";
            parser.clear();

        }
        /// <summary>
        /// savefile to save the code
        /// </summary>
        /// <returns></returns>
        private bool saveFile()
        {
            string code = textBox1.Text;
            if (_filePath == null)
            {
                SaveFileDialog saveFileDialog = new SaveFileDialog();
                saveFileDialog.Filter = "gpl files (*.gpl)|*.gpl|All files (*.*)|*.*";
                saveFileDialog.Title = "Save GPL";
                saveFileDialog.ShowDialog();

                if (saveFileDialog.FileName != "")
                {
                    _filePath = saveFileDialog.FileName;
                    using (StreamWriter sw = new StreamWriter(_filePath))
                        sw.WriteLine(code);
                    _fileSaved = true;
                    return true;
                }
                else
                {
                    return false;
                }
            }

            else
            {
                using (StreamWriter sw = new StreamWriter(_filePath))
                    sw.WriteLine(code);
                _fileSaved = true;
                return true;
            }
        }
        /// <summary>
        /// Textbox to command the case
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBox2_KeyDown(object sender, KeyEventArgs e)
        {
            string command = textBox2.Text.ToLower().Split(',')[0];
            if (e.KeyCode == Keys.Enter)
            {
                switch (command)
                {
                    case "clear":
                        g.Clear(Color.Gray);
                        parser.clear();
                        break;
                    case "reset":
                        g.Clear(Color.Gray);
                        parser.clear();
                        textBox1.Text = "";
                        textBox2.Text = "";
                        break;
                    case "run":
                        run_Click(sender, e);
                        break;
                    default:
                        MessageBox.Show("Unknown Command", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        break;
                }
                e.SuppressKeyPress = true;
            }

        }
        /// <summary>
        ///Opening the saved file
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!_fileSaved)
            {
                DialogResult dialogResult = MessageBox.Show("Save current file before proceeding?", "Changes not saved", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);
                if (dialogResult == DialogResult.Yes)
                {
                    if (!saveFile())
                        return;
                }
                else if (dialogResult == DialogResult.Cancel)
                {
                    return;
                }
            }
            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                openFileDialog.InitialDirectory = "c:\\";
                openFileDialog.Filter = "gpl files (*.gpl)|*.gpl|All files (*.*)|*.*";
                openFileDialog.FilterIndex = 2;
                openFileDialog.RestoreDirectory = true;

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    _filePath = openFileDialog.FileName;

                    var fileStream = openFileDialog.OpenFile();

                    using (StreamReader reader = new StreamReader(fileStream))
                    {
                        textBox1.Text = reader.ReadToEnd();
                    }

                    _fileSaved = true;
                }
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!_fileSaved)
            {
                DialogResult dialogResult = MessageBox.Show("Save current file before proceeding?", "Changes not saved", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);
                if (dialogResult == DialogResult.Yes)
                {
                    if (!saveFile())
                        return;
                }
                else if (dialogResult == DialogResult.Cancel)
                {
                    return;
                }
            }
            Application.Exit();
        }

        private void textProgramBox_TextChanged(object sender, EventArgs e)
        {
            _fileSaved = false;
        }


    }
}