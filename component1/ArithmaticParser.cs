﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace component1
{
    internal class ArithmaticParser
    {
        /// <summary>
        /// Creating Parse and expression
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        public int Parse(string expression)
        {
            string[] operands = expression.Split(new string[] { "+", "-", "*", "/", "%" },
                StringSplitOptions.RemoveEmptyEntries);
            if (operands.Length == 0)
            {
                MessageBox.Show("Insufficient Parameters", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (operands.Length == 1)
            {
                return Variable.Find(operands[0]);
            }
            else 
            {
                string Operator = expression.Replace(operands[0], "").Replace(operands[1], "").Trim();
                if (Operator.Length == 0)
                {
                    MessageBox.Show("Undefined Constant", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                int val1 = Variable.Find(operands[0].Trim());
                int val2 = Variable.Find(operands[1].Trim());

                switch (Operator)
                {
                    case "+":
                        return val1 + val2;
                    case "-":
                        return val1 - val2;
                    case "*":
                        return val1 * val2;
                    case "/":
                        return val1 / val2;
                    case "%":
                        return val1 % val2;                
                }
            }
            return 0;
        }
    }
}
