﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace component1
{
    /// <summary>
    /// creating line command to generate the required command \
    /// to execute properly when implemented
    /// </summary>
    public class Line : Shape
    {
        int x2, y2;
        public Line():base()
        {

        }
        /// <summary>
        /// implementing constructor in circle class to generate colors 
        /// and a pathway to draw circle
        /// Overloading Constructor and call overloaded base constructor
        /// </summary>
        /// <param name="color">Color Of Pen</param>
        /// <param name="fill">Inner Fill Shapes</param>
        /// <param name="x">X-axis Coordinate</param>
        /// <param name="y">Y-axis Coordinate</param>
        /// <param name="radius">Radius Of Circle</param>
        public Line(Color color, bool fill, int xaxis, int yaxis, int x2, int y2):base(color, fill, xaxis, yaxis)
        {
            this.x2 = x2;
            this.y2 = y2;
        }

        public override double calcArea()
        {
            throw new NotImplementedException();
        }

        public override double calcPerimeter()
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// implementing graphics and 
        /// creating darw method which is inherited from base class 
        /// created if else statement to color or pen accordingly
        /// </summary>
        public override void draw(Graphics g)
        {
            Pen p = new Pen(color, 3);
            g.DrawLine(p, x, y, x2, y2);
        }

        public override void set(Color color, bool fillState, params int[] list)
        {
            base.set(color, fillState, list[0], list[1]);
            this.x2 = list[2];
            this.y2 = list[3];
        }
    }
}