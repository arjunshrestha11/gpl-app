﻿  using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace component1
{
     public class Variable
    {
         public static Dictionary<string, int> _vars = new Dictionary<string, int>();
        public static Dictionary<string, int> Vars 
        { 
            get { return _vars; }
            set { _vars = value; }
        }
        public static void Clear()
        { 
            _vars = new Dictionary<string, int>(); 
        }
        public static int Find(string var) //search through defined variables
        { 
            int result; 
            if (!_vars.TryGetValue(var, out result))
            {
                try
                {
                    result = int.Parse(var);
                }
                catch (FormatException)
                {
                    MessageBox.Show("Undefined Constant", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            return result;

        }
    }
}
