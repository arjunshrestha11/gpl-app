﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace component1
{
    /// <summary>
    /// public Abstract class Shape which declares the factory method
    /// </summary>
    public abstract class Shape:IShapes
    {   /// <summary>
    /// color of shape
    /// </summary>
        protected Color color;
        /// <summary>
        /// flashing color of the shape
        /// </summary>
        protected string flash;
        protected bool fill;
        protected int x, y;
        
        /// <summary>
        /// Default constructor
        /// </summary>
        public Shape()
        {
            color = Color.Black;
            x = y = 100;
        }
        /// <summary>
        /// Overloading constructor with parameters
        /// </summary>
        /// <param name="color">Color Of Pen</param>
        /// <param name="fill">Inner Fill Shapes</param>
        /// <param name="x">X-axis Coordinate</param>
        /// <param name="y">Y-axis Coordinate</param>

        public Shape(Color color, bool fill, int x, int y)
        {
            this.color = color;
            this.fill = fill;
            this.x = x;
            this.y = y;
        }
        /// <summary>
        /// creatinh shape to shape the command interface
        /// </summary>
        /// <param name=""></param>
        /// 
         public Shape(string flash , bool fill, int x, int y)
        
        {
            this.flash = flash;
            this.fill = fill;
            this.x = x;
            this.y = y;
        }

    public abstract void draw(Graphics g);
        public abstract double calcArea();
        public abstract double calcPerimeter();
        /// <summary>
        /// Override set method derived from base class
        /// </summary>
        /// <param name="color">Color of pen</param>
        /// <param name="fill">Inner fill shapes</param>
        /// <param name="list">stores number of arguments</param>
        public virtual void set(Color color, bool fill, params int[] list)
        {
            this.color = color;
            this.fill = fill;
            this.x = list[0];
            this.y = list[1];
        }
        /// <summary>
        /// name="color"
        /// name="fill"
        /// name="list"
        /// </summary>
        /// <returns></returns>
        public virtual void set(string flash, bool fill, params int[] list)
        {
            this.flash = flash;
            this.fill = fill;
            this.x = list[0];
            this.y = list[1];
        }

        public override string ToString()
        {
            return base.ToString() + "    " + this.x + "," + this.y + " : ";
        }
    }
}
