﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace component1
{
    /// <summary>
    /// Class Traingle inherits base class Shape
    /// </summary>
    public class Triangle : Shape
    {
        int x2, y2, x3, y3;
        double a, b, c;
        /// <summary>
        /// Call base class constructor
        /// </summary>
        public Triangle() : base()
        {

        }
        /// <summary>
        /// Overloading Constructor and call overloaded base constructor
        /// </summary>
        /// <param name="color">Color Of Pen</param>
        /// <param name="fill">Inner Fill Shapes</param>
        /// <param name="x">X-axis Coordinate</param>
        /// <param name="y">Y-axis Coordinate</param>
        /// <param name="x2">Side1 of Traingle</param>
        /// <param name="y2">Side2 of Traingle</param>
        /// <param name="x3">Side3 of Traingle</param>
        /// <param name="y3">Side4 of Traingle</param>
        /// 
        public Triangle(Color color, bool fill, int x, int y, int x2, int y2, int x3, int y3) : base(color, fill, x, y)
        {
            this.x2 = x2;
            this.y2 = y2;
            this.x3 = x3;
            this.y3 = y3;
            this.a = Math.Sqrt((x2 - x) ^ 2 + (y2 - y) ^ 2);
            this.b = Math.Sqrt((x3 - x2) ^ 2 + (y3 - y2) ^ 2);
            this.c = Math.Sqrt((x - x3) ^ 2 + (y - y3) ^ 2);
        }
        /// <summary>
        /// Creating flash to display multipule colors
        /// </summary>
        /// <param name="color"></param>
        /// <param name="fill"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="x2"></param>
        /// <param name="y2"></param>
        /// <param name="x3"></param>
        /// <param name="y3"></param>
        public Triangle(string  flash, bool fill, int x, int y, int x2, int y2, int x3, int y3) : base(flash, fill, x, y)
        {
            this.x2 = x2;
            this.y2 = y2;
            this.x3 = x3;
            this.y3 = y3;
            this.a = Math.Sqrt((x2 - x) ^ 2 + (y2 - y) ^ 2);
            this.b = Math.Sqrt((x3 - x2) ^ 2 + (y3 - y2) ^ 2);
            this.c = Math.Sqrt((x - x3) ^ 2 + (y - y3) ^ 2);
        }
        /// <summary>
        /// Recent implementation of Set method that is inherited from a base class.
        /// </summary>
        /// <param name="color">Color of pen</param>
        /// <param name="fill">Inner fill shapes</param>
        /// <param name="list">stores number of arguments</param>
        public override void set(Color color, bool fill, params int[] list)
        {
            // list[2] is x, list[3] is y, list[4],list[5] is radius
            base.set(color, fill, list[0], list[1]);
            this.x2 = list[2];
            this.y2 = list[3];
            this.x3 = list[4];
            this.y3 = list[5];
        }
        /// <summary>
        /// Creating void set string flash
        /// </summary>
        /// <param name="flash"></param>
        /// <param name="fill"></param>
        /// <param name="list"></param>
        public override void set(string flash, bool fill, params int[] list)
        {
            // list[2] is x, list[3] is y, list[4],list[5] is radius
            base.set(flash, fill, list[0], list[1]);
            this.x2 = list[2];
            this.y2 = list[3];
            this.x3 = list[4];
            this.y3 = list[5];
        }
        /// <summary>
        /// New implementation of Draw method that is inherited from a base class.
        /// </summary>
        public override void draw(Graphics g)
        {
            Point[] points = new Point[3];
            points[0].X = this.x;
            points[0].Y = this.y;
            points[1].X = this.x2;
            points[1].Y = this.y2;
            points[2].X = this.x3;
            points[2].Y = this.y3;
            if (fill)
            {
                if (flash != null)
                {
                    Thread newThread;
                    switch (flash)
                    {
                        case "redgreen":
                            newThread = new(() => StartFlashing(g,points, Color.Red, Color.Green));
                            newThread.IsBackground = true;
                            newThread.Start();
                            break;
                        case "blueyellow":
                            newThread = new(() => StartFlashing(g,points ,Color.Blue, Color.Yellow));
                            newThread.IsBackground = true;
                            newThread.Start();
                            break;
                        case "blackwhite":
                            newThread = new(() => StartFlashing(g,points, Color.Black, Color.White));
                            newThread.IsBackground = true;
                            newThread.Start();
                            break;
                        default:
                            MessageBox.Show("Invalid color value", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            break;
                    }
                }
                else
                {
                    SolidBrush b = new SolidBrush(color);
                    g.FillPolygon(b, points);
                }
            }
            else
            {
                Pen p = new Pen(color, 2);
                g.DrawPolygon(p, points);
            }

        }

        public override double calcArea()
        {
            return (1 / 2) * Math.Abs(x * (y2 - y3) + x2 * (y3 - y) + x3 * (y - y2));
        }

        public override double calcPerimeter()
        {
            return a + b + c;
        }
        private void StartFlashing(Graphics g,Point[]points ,Color first, Color second)
        {
            bool flag = false;
            while (true)
            {
                lock (g)
                {
                    if (flag == false)
                    {
                        SolidBrush b = new(first);
                        g.FillPolygon(b, points);
                        flag = true;
                    }
                    else
                    {
                        SolidBrush b = new(second);
                        g.FillPolygon(b, points);
                        flag = false;
                    }
                }
                Thread.Sleep(1000);
            }
        }


        public override string ToString()
        {
            return base.ToString() + " " + this.x2 + " " + this.y2 + " " + this.x3 + " " + this.y3;
        }
    }
}
