﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace component1
{
    /// <summary>
    /// Creating logic parser
    /// </summary>
    class LogicParser
    {
        ArithmaticParser arithmaticParser = new ArithmaticParser();
        /// <summary>
        /// Parse to parse the string expression
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        public bool Parse(string expression)
        {
            string[] operands = expression.Split(new string[] { "==", "!=", "<=", ">=", "<", ">" },
                StringSplitOptions.RemoveEmptyEntries);
            if (operands.Length == 0)
            {
                MessageBox.Show("Undefined Constant", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (operands.Length == 1)
            {
                return false;
            }
            else
            {
                string Operator = expression.Replace(operands[0], "").Replace(operands[1], "").Trim();
                if (Operator.Length == 0)
                {
                    MessageBox.Show("Undefined Constant", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                int val1 = arithmaticParser.Parse(operands[0].Trim());
                int val2 = arithmaticParser.Parse(operands[1].Trim());

                switch (Operator)
                {
                    case "==":
                        return val1 == val2;
                    case "!=":
                        return val1 != val2;
                    case "<":
                        return val1 < val2;
                    case ">":
                        return val1 > val2;
                    case "<=":
                        return val1 <= val2;
                    case ">=":
                        return val1 >= val2;
                }
            }
            return false;
        }
    }
}
