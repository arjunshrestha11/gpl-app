﻿namespace component1
{
    /// <summary>
    /// defining circle which  ineherits shape
    ///  creating radius inside class 
    /// </summary>
    public class Circle : Shape 
    {
        /// <summary>
        /// calling base class contructor
        /// </summary>
        int radius;

        public Circle() : base()
        {

        }
        /// <summary>
        /// implementing constructor in circle class to generate colors 
        /// and a pathway to draw circle
        /// Overloading Constructor and call overloaded base constructor
        /// </summary>
        /// <param name="color">Color Of Pen</param>
        /// <param name="fill">Inner Fill Shapes</param>
        /// <param name="x">X-axis Coordinate</param>
        /// <param name="y">Y-axis Coordinate</param>
        /// <param name="radius">Radius Of Circle</param>

        public Circle(Color color, bool fill, int x, int y, int radius) : base(color, fill,x, y)
        {
            this.radius = radius;
        }
        /// <summary>
        /// Creating circle flash to display multicolours
        /// </summary>
        /// <param name="flash"></param>
        /// <param name="fill"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="radius"></param>
        public Circle(string flash, bool fill, int x, int y, int radius) : base(flash, fill, x, y)
        {
            this.radius = radius;
        }
        /// <summary>
        /// implementation of Set method which is inherited from a base class.
        /// </summary>
        /// <param name="color">Color of pen</param>
        /// <param name="fill">Inner fill shapes</param>
        /// <param name="list">stores number of arguments</param>
        public override void set(Color color, bool fill, params int[] list)
        {
            base.set(color,fill, list[0], list[1]);
            this.radius = list[2];
        }
        /// <summary>
        /// Creation of flash,fill and list.
        /// </summary>
        /// <param name="flash"></param>
        /// <param name="fill"></param>
        /// <param name="list"></param>
        public override void set(string flash, bool fill, params int[] list)
        {
            base.set(flash, fill, list[0], list[1]);
            this.radius = list[2];
        }
        /// <summary>
        /// Implementing graphics and 
        /// creating darw method which is inherited from base class 
        /// created if else statement to color or pen accordingly
        /// </summary>
        ///
        public override void draw(Graphics g)
        {
            if (fill)
            {
                if (flash != null)
                {
                    Thread newThread;
                    switch (flash)
                    {
                        case "redgreen":
                            newThread = new(() => StartFlashing(g, Color.Red, Color.Green));
                            newThread.IsBackground = true;
                            newThread.Start();
                            break;
                        case "blueyellow":
                            newThread = new(() => StartFlashing(g, Color.Blue, Color.Yellow));
                            newThread.IsBackground = true;
                            newThread.Start();
                            break;
                        case "blackwhite":
                            newThread = new(() => StartFlashing(g, Color.Black, Color.White));
                            newThread.IsBackground = true;
                            newThread.Start();
                            break;
                        default:
                            MessageBox.Show("Invalid color value", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            break;
                    }
                }
                else
                {
                    SolidBrush b = new SolidBrush(color);
                    g.FillEllipse(b, x, y, radius * 2, radius * 2);
                }
                
            }
            else {
                Pen p = new Pen(color, 2);
                g.DrawEllipse(p, x, y, radius * 2, radius * 2);
            }
            
        }
        // claculating the radius of circle
        public override double calcArea()
        {
            return Math.PI * (radius ^ 2);
        }

        public override double calcPerimeter()
        {
            return 2 * Math.PI * radius;
        }
        /// <summary>
        /// Creating Start Flashing to display Multiple Colours
        /// </summary>
        /// <param name="g"></param>
        /// <param name="first"></param>
        /// <param name="second"></param>
        private void StartFlashing(Graphics g, Color first, Color second)
        {
            bool flag = false;
            while (true)
            {
                lock (g)
                {
                    if (flag == false)
                    {
                        SolidBrush b = new(first);
                        g.FillEllipse(b, x, y, radius * 2, radius * 2);
                        flag = true;
                    }
                    else
                    {
                        SolidBrush b = new(second);
                        g.FillEllipse(b, x, y, radius * 2, radius * 2);
                        flag = false;
                    }
                }
                Thread.Sleep(500);
            }
        }
        public override string ToString()
        {
            return base.ToString() + " " + this.radius;
        }
    }

}
