﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace component1
{
    /// <summary>
    /// Creating function class to use function and run the code
    /// </summary>
    class Function
    {
        private static Dictionary<string, int> _funcs = new Dictionary<string, int>();
        public static Dictionary<string, int> Funcs
        {
            get
            {
                return _funcs;
            }
            set
            {
                _funcs = value;
            }
        }
        /// <summary>
        /// Clear function to clear the command
        /// </summary>
        public static void Clear()
        {
            _funcs = new Dictionary<string, int>();
        }
        /// <summary>
        /// Search function to find the value
        /// </summary>
        /// <param name="var"></param>
        /// <returns></returns>
        public static int SearchFunction(string var)
        {
            int result;
            if (!_funcs.TryGetValue(var, out result))
            {
                MessageBox.Show("Undefined function", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return result;
        }
    }

}
