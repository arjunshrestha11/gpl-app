﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace component1
{
    /// <summary>
    /// Class Rectangle inherits base class Shape
    /// </summary>
    public class Rectangle : Shape
    {
        int width, height;
        /// <summary>
        /// calling base class constructor
        /// </summary>
        public Rectangle() : base()
        {

        }  
        /// <summary>
        /// Overloading Constructor and call overloaded base constructor
        /// </summary>
        /// <param name="color">Color Of Pen</param>
        /// <param name="fill">Inner Fill Shapes</param>
        /// <param name="x">X-axis Coordinate</param>
        /// <param name="y">Y-axis Coordinate</param>
        /// <param name="width">width of Rectangle</param>
        /// <param name="height">height of Rectangle</param>

        public Rectangle(Color color, bool fill,int x, int y, int width, int height) : base(color,fill, x, y)
        {
            this.width = width;
            this.height = height;
        }
        /// <summary>
        /// Creating rectangle flash to display multicolours
        /// </summary>
        /// <param name="flash"></param>
        /// <param name="fill"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        public Rectangle(string flash, bool fill, int x, int y, int width, int height) : base(flash, fill, x, y)
        {
            this.width = width;
            this.height = height;
        }
        // <summary>
        /// current implementation of Set method that is inherited from a base class.
        /// </summary>
        /// <param name="color">Color of pen</param>
        /// <param name="fill">Inner fill shapes</param>
        /// <param name="list">stores number of arguments</param>
        public override void set(Color color, bool fill,params int[] list)
        {
            base.set(color, fill, list[0], list[1]);
            this.width = list[2];
            this.height = list[3];
        }
        /// <summary>
        /// creating void to set flash
        /// </summary>
        /// <param name="flash"></param>
        /// <param name="fill"></param>
        /// <param name="list"></param>
        public override void set(string flash, bool fill, params int[] list)
        {
            base.set(flash, fill, list[0], list[1]);
            this.width = list[2];
            this.height = list[3];
        }
        /// <summary>
        /// implementation of Draw method  inherited from a base class.
        /// </summary>
        public override void draw(Graphics g)
        {
            if (fill)
            {
                if (flash != null)
                {
                    Thread newThread;
                    switch (flash)
                    {
                        case "redgreen":
                            newThread = new(() => StartFlashing(g, Color.Red, Color.Green));
                            newThread.IsBackground = true;
                            newThread.Start();
                            break;
                        case "blueyellow":
                            newThread = new(() => StartFlashing(g, Color.Blue, Color.Yellow));
                            newThread.IsBackground = true;
                            newThread.Start();
                            break;
                        case "blackwhite":
                            newThread = new(() => StartFlashing(g, Color.Black, Color.White));
                            newThread.IsBackground = true;
                            newThread.Start();
                            break;
                        default:
                            MessageBox.Show("Invalid color value", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            break;
                    }
                }
                else
                {

                    SolidBrush b = new SolidBrush(color);
                    g.FillRectangle(b, x, y, width, height);
                }
            }
            else {
                Pen p = new Pen(color, 2);
                g.DrawRectangle(p, x, y, width, height);
            }     
        }

        public override double calcArea()
        {
            return width * height;
        }

        public override double calcPerimeter()
        {
            return 2 * (width + height);
        }
        private void StartFlashing(Graphics g, Color first, Color second)
        {
            bool flag = false;
            while (true)
            {
                lock (g)
                {
                    if (flag == false)
                    {
                        SolidBrush b = new(first);
                        g.FillRectangle(b, x, y, width,height);
                        flag = true;
                    }
                    else
                    {
                        SolidBrush b = new(second);
                        g.FillRectangle(b, x, y, width, height);
                        flag = false;
                    }
                }
                Thread.Sleep(1000);
            }
        }

        public override string ToString()
        {
            return base.ToString() + " " + this.width + " " + this.height;
        }
    }
}
