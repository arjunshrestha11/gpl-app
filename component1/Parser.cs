﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace component1
{
    /// <summary>
    /// 
    /// </summary>
    public class Parser
    {
        private Graphics g;
        public Parser()
        {

        }

        public Parser(Graphics g)
        {
            this.g = g;
        }
        /// <summary>
        /// implemented static int,color and bool to
        /// generate axis and color to the field of the classs
        /// </summary>
        private static int x = 0, y = 0;
        private static Color color = Color.Black;
        private static bool fill;
        private string? flash = null;
        static ArithmaticParser arithmaticParser = new ArithmaticParser();

        public int X
        {
            get { return x; }
        }
        public int Y
        {
            get { return y; }
        }
        public Color Color
        {
            get { return color; }
        }
        public bool Fill
        {
            get { return fill; }
        }

        /// <summary>
        /// created new method to clear the programm when clear button  is run
        /// </summary>
        public void clear()
        {
            x = 0;
            y = 0;
            color = Color.Black;
            fill = false;
            flash = null;
        }
        /// <summary>
        /// parse to generate the given code in command line
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="g"></param>
        public void parse(string cmd)
        {
            ShapeFactory factory = new ShapeFactory();
            try
            {
                char[] seperator = new char[] { ' ' };
                string[] args = cmd.Split(seperator, 2);
                string command = args[0];
                string parameters = "";
                if (args.Length > 1)
                {
                    parameters = args[1];
                }


                switch (command.ToLower())
                {
                    case "":
                        break;
                    case "circle":
                        int radius = arithmaticParser.Parse(parameters.Trim());
                        Shape circle = factory.getShape("circle");
                        if (flash != null)
                        {
                            circle.set(flash, fill, x, y, radius);
                        }
                        else
                        {
                            circle.set(color, fill, x, y, radius);
                        }
                        circle.draw(g);
                        break;
                    case "rectangle":
                        int width = arithmaticParser.Parse(parameters.Split(',')[0].Trim());
                        int height = arithmaticParser.Parse(parameters.Split(',')[1].Trim());
                        Shape rectangle = factory.getShape("rectangle");
                        if (flash != null)
                        {
                            rectangle.set(flash, fill, x, y, width, height);
                        }
                        else
                        {
                            rectangle.set(color, fill, x, y, width, height);
                        }
                        rectangle.draw(g);
                        break;
                    case "triangle":
                        int x2 = arithmaticParser.Parse(parameters.Split(',')[0].Trim());
                        int y2 = arithmaticParser.Parse(parameters.Split(',')[1].Trim());
                        int x3 = arithmaticParser.Parse(parameters.Split(',')[2].Trim());
                        int y3 = arithmaticParser.Parse(parameters.Split(',')[3].Trim());
                        Shape triangle = factory.getShape("triangle");
                        if (flash != null)
                        {
                            triangle.set(flash, fill, x, y, x2, y2, x3, y3);
                        }
                        triangle.set(color, fill, x, y, x2, y2, x3, y3);
                        triangle.draw(g);
                        break;
                    case "moveto":
                        x = arithmaticParser.Parse(parameters.Split(',')[0].Trim());
                        y = arithmaticParser.Parse(parameters.Split(',')[1].Trim());
                        break;
                    case "color":
                        try
                        {
                            if (parameters.Split(',').Length == 1)
                            {
                                if (parameters == "redgreen" || parameters == "blueyellow" || parameters == "blackwhite")
                                {
                                    flash = parameters;
                                }

                                color = Color.FromName(parameters.Trim());
                            }
                            else if (parameters.Split(',').Length == 3)
                            {
                                int red = arithmaticParser.Parse(parameters.Split(',')[0].Trim());
                                int green = arithmaticParser.Parse(parameters.Split(',')[1].Trim());
                                int blue = arithmaticParser.Parse(parameters.Split(',')[2].Trim());
                                color = Color.FromArgb(255, red, green, blue);
                            }
                            else if (parameters.Split(',').Length == 4)
                            {
                                int alpha = arithmaticParser.Parse(parameters.Split(',')[0].Trim());
                                int red = arithmaticParser.Parse(parameters.Split(',')[1].Trim());
                                int green = arithmaticParser.Parse(parameters.Split(',')[2].Trim());
                                int blue = arithmaticParser.Parse(parameters.Split(',')[3].Trim());
                                color = Color.FromArgb(alpha, red, green, blue);
                            }
                            else
                            {
                                MessageBox.Show("Invalid Color Value", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }


                        }
                        catch (FormatException)
                        {
                            MessageBox.Show("Invalid Color Value", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        break;
                    case "fill":
                        {
                            fill = bool.Parse(parameters.Split(',')[0].Trim());
                            break;
                        }
                    case "var":
                        char[] separator = { ' ' };
                        string[] expression = parameters.Split(separator, 2);
                        if (Variable.Vars.ContainsKey(expression[0].Trim()))
                        {
                            MessageBox.Show("Undefined Constant", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        else
                        {
                            try
                            {
                                Variable.Vars.Add(expression[0].Trim(), arithmaticParser.Parse(expression[1].Trim()));
                            }
                            catch (Exception)
                            {
                                Variable.Vars.Add(expression[0].Trim(), 0);
                            }
                        }
                        break;


                    case "clear":
                        {
                            g.Clear(Color.Gray);
                            break;
                        }
                    case "drawto":
                        int xaxis2 = arithmaticParser.Parse(parameters.Split(',')[0].Trim());
                        int yaxis2 = arithmaticParser.Parse(parameters.Split(',')[1].Trim());
                        Shape line = factory.getShape("line");
                        line.set(color, fill, x, y, xaxis2, yaxis2);
                        line.draw(g);
                        break;
                    default:
                        if (command.Contains('='))
                        {
                            string[] splits = command.Split('=');
                            Variable.Vars[splits[0].Trim()] = arithmaticParser.Parse(splits[1].Trim());
                        }
                        else if (parameters.Split(',')[0].Trim().Contains('='))
                        {
                            Variable.Vars[command] = arithmaticParser.Parse(parameters.Split(',')[0].Trim().Substring(1).Trim());
                        }
                        else
                            MessageBox.Show("Unknown Command", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        break;
                }
            }
            catch (FormatException)
            {
                MessageBox.Show($"Invalid argument type", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
