﻿using component1;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Drawing;

namespace ShapeTest
{
    [TestClass]
    public class TestCommand
    {
        readonly Parser parser = new Parser();
        [TestMethod]
        public void MoveToTest()
        {
            string cmd = "moveto 50, 20";
            parser.parse(cmd);
            Assert.AreEqual(50, parser.X);
            Assert.AreEqual(20, parser.Y);
        }
        public void FillTest()
        {
            string cmd = "fill true";
           parser.parse(cmd);
            Assert.AreEqual(true, parser.Fill);
        }
        public void ColorTest()
        {
            string cmd = "Color red";
            parser.parse(cmd);
            Assert.AreEqual(Color.Red, parser.Color);
        }
        public void ClearTest()
        {
            parser.clear();
            Assert.AreEqual(0, parser.X);
            Assert.AreEqual(0, parser.Y);
            Assert.AreEqual(Color.Black, parser.Color);
            Assert.AreEqual(false, parser.Fill);
        }
        public void VarTest()
        {
            string cmd = "Var i";
            parser.parse(cmd);
            Assert.IsTrue(Variable.Vars.ContainsKey("i"));
        }

    }
}
