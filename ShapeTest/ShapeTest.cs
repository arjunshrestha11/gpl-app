﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using component1;

namespace ShapeTest
{
    [TestClass]
    public class ShapeTest
    {
        [TestMethod]
        public void CircleTest()
        {
            String cmd = "circle 10";
            Parser parser = new Parser();
            ShapeFactory factory = new ShapeFactory();
            String[] parsedCmd = cmd.ToLower().Split(' ');
            Shape circle = factory.getShape(parsedCmd[0].Trim().ToLower());
            Assert.IsInstanceOfType(circle, typeof(Circle));
        }
        [TestMethod]
        public void RectangleTest()
        {
            String cmd = "rectangle 50, 100";
            Parser parser = new Parser();
            ShapeFactory factory = new ShapeFactory();
            String[] parsedCmd = cmd.ToLower().Split(' ');
            Shape rectangle = factory.getShape(parsedCmd[0].Trim().ToLower());
            Assert.IsInstanceOfType(rectangle, typeof(Rectangle));
        }
        [TestMethod]
        public void TriangleTest()
        {
            String cmd = "triangle 100, 150, 150, 150";
            Parser parser = new Parser();
            ShapeFactory factory = new ShapeFactory();
            String[] parsedCmd = cmd.ToLower().Split(' ');
            Shape triangle = factory.getShape(parsedCmd[0].Trim().ToLower());
            Assert.IsInstanceOfType(triangle, typeof(Triangle));
        }
        [TestMethod]
        public void DrawToTest()
        {
            String cmd = "line 100, 150";
            Parser parser = new Parser();
            ShapeFactory factory = new ShapeFactory();
            String[] parsedCmd = cmd.ToLower().Split(' ');
            Shape line = factory.getShape(parsedCmd[0].Trim().ToLower());
            Assert.IsInstanceOfType(line, typeof(Line));
        }
        [TestMethod]
        public void InvalidShapeTest()
        {
            String cmd = "invalid 100, 150, 150, 150";
            Parser parser = new Parser();
            ShapeFactory factory = new ShapeFactory();
            String[] parsedCmd = cmd.ToLower().Split(' ');
            try
            {
                Shape triangle = factory.getShape(parsedCmd[0].Trim().ToLower());
                Assert.Fail("No exception thrown");
            }
            catch (Exception ex)
            {
                Assert.IsTrue(ex is ArgumentException);
            }
        }
    }
}